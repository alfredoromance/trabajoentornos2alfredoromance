package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*
		 * El error esta en la instruccion del for, debido a que al poner <= 
		 * lo que hace eso es sumar uno mas a la i
		 * La solucion seria quitar el =
		 */
		
		/*
		 * Viendo el debug entendemos que el programa al meterle una cadena 
		 * nos cuenta el numero de vocales y cifras, esas variables se mantendran en 0 
		 * si no metemos ninguna, mientras que la i aumenta segun los caracteres de la cadena
		 */
		
		for(int i = 0 ; i < cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
