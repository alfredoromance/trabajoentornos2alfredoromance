package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		
		/*
		 * El problema esta en el caracter "27", ya que en la tabla ASCII aparece vacio, es porque el caracter 27 representa 
		 * un caracter que debe ser leido con el codigo ANSI, por eso el programa al leer una cadena no reconoce ese caracter
		 * la solucion seria poner otro que la tabla ASCII si pueda leer
		 */
		char caracter = 27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
