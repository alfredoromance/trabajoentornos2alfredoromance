package gui;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Button;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class panel  extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 *  @Alfredo romance y @08/01/2018.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					panel frame = new panel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public panel() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Clase\\Desktop\\Eclipse-AlfredoRomance\\IMAGENES\\descarga.png"));
		setForeground(Color.ORANGE);
		setBackground(Color.BLACK);
		setTitle("FORMULARIO CLINICA");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 970, 476);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(SystemColor.textHighlight);
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarcomo = new JMenuItem("guardarcomo");
		mnArchivo.add(mntmGuardarcomo);
		
		JMenu mnEdicion = new JMenu("Edicion");
		menuBar.add(mnEdicion);
		
		JMenuItem mntmCopiar = new JMenuItem("copiar");
		mnEdicion.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("pegar");
		mnEdicion.add(mntmPegar);
		
		JMenuBar menuBar_1 = new JMenuBar();
		menuBar.add(menuBar_1);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.controlShadow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JCheckBox checkBox = new JCheckBox("Dr mingi");
		checkBox.setBounds(362, 145, 89, 33);
		contentPane.add(checkBox);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Dr cardero");
		chckbxNewCheckBox.setBounds(362, 102, 89, 25);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxDrPon = new JCheckBox("Dr. pon");
		chckbxDrPon.setBounds(362, 58, 89, 25);
		contentPane.add(chckbxDrPon);
		
		JCheckBox chckbxDrPipa = new JCheckBox("Dr pipa");
		chckbxDrPipa.setBounds(362, 8, 89, 25);
		contentPane.add(chckbxDrPipa);
		
		JCheckBox chckbxDrPipo = new JCheckBox("Dr pipo");
		chckbxDrPipo.setBounds(362, 32, 89, 25);
		contentPane.add(chckbxDrPipo);
		
		JCheckBox chckbxDrPin = new JCheckBox("Dr. pin");
		chckbxDrPin.setBounds(362, 81, 89, 25);
		contentPane.add(chckbxDrPin);
		
		JCheckBox chckbxDrPongo = new JCheckBox("Dr. pongo");
		chckbxDrPongo.setBounds(362, 123, 89, 25);
		contentPane.add(chckbxDrPongo);
		
		JList list = new JList();
		list.setBounds(391, 126, 1, 1);
		contentPane.add(list);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 32, 354, 240);
		contentPane.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 18));
		textArea.setForeground(Color.YELLOW);
		textArea.setBackground(Color.BLACK);
		scrollPane.setViewportView(textArea);
		
		JButton btnNewButton = new JButton("Enviar");
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setBackground(Color.ORANGE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(824, 351, 97, 25);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Reset");
		btnNewButton_1.setBackground(Color.PINK);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(508, 351, 97, 25);
		contentPane.add(btnNewButton_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Traumatologia");
		rdbtnNewRadioButton.setBounds(343, 320, 127, 25);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton radioButton = new JRadioButton("Oftalmologia");
		radioButton.setBounds(227, 320, 127, 25);
		contentPane.add(radioButton);
		
		JSlider slider = new JSlider();
		slider.setForeground(Color.ORANGE);
		slider.setBackground(Color.RED);
		slider.setBounds(610, 246, 200, 26);
		contentPane.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(741, 158, 45, 25);
		contentPane.add(spinner);
		
		JTextPane txtpnIntroduceTuEdad = new JTextPane();
		txtpnIntroduceTuEdad.setBackground(Color.BLACK);
		txtpnIntroduceTuEdad.setForeground(Color.ORANGE);
		txtpnIntroduceTuEdad.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnIntroduceTuEdad.setText("Edad del paciente");
		txtpnIntroduceTuEdad.setBounds(790, 158, 150, 25);
		contentPane.add(txtpnIntroduceTuEdad);
		
		JTextPane txtpnInformacionPersonal = new JTextPane();
		txtpnInformacionPersonal.setFont(new Font("Times New Roman", Font.PLAIN, 29));
		txtpnInformacionPersonal.setForeground(Color.YELLOW);
		txtpnInformacionPersonal.setBackground(Color.BLACK);
		txtpnInformacionPersonal.setText("Informacion Personal: ");
		txtpnInformacionPersonal.setBounds(0, 0, 354, 33);
		contentPane.add(txtpnInformacionPersonal);
		
		JRadioButton rdbtnUrologia = new JRadioButton("Urologia");
		rdbtnUrologia.setBounds(126, 320, 127, 25);
		contentPane.add(rdbtnUrologia);
		
		JRadioButton rdbtnCarido = new JRadioButton("Carido");
		rdbtnCarido.setBounds(36, 320, 127, 25);
		contentPane.add(rdbtnCarido);
		
		textField = new JTextField();
		textField.setBounds(545, 19, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JTextPane nombre = new JTextPane();
		nombre.setText("nombre");
		nombre.setForeground(Color.ORANGE);
		nombre.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		nombre.setBackground(Color.BLACK);
		nombre.setBounds(475, 8, 72, 33);
		contentPane.add(nombre);
		
		JTextPane txtpnApellidos = new JTextPane();
		txtpnApellidos.setText("Apellidos");
		txtpnApellidos.setForeground(Color.ORANGE);
		txtpnApellidos.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnApellidos.setBackground(Color.BLACK);
		txtpnApellidos.setBounds(475, 50, 72, 33);
		contentPane.add(txtpnApellidos);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(545, 61, 116, 22);
		contentPane.add(textField_1);
		
		JTextPane txtpnDireccion = new JTextPane();
		txtpnDireccion.setText("direccion");
		txtpnDireccion.setForeground(Color.ORANGE);
		txtpnDireccion.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnDireccion.setBackground(Color.BLACK);
		txtpnDireccion.setBounds(475, 89, 72, 33);
		contentPane.add(txtpnDireccion);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(545, 100, 209, 22);
		contentPane.add(textField_2);
		
		JTextPane txtpnPersonaDeContacto = new JTextPane();
		txtpnPersonaDeContacto.setText("Persona de contacto");
		txtpnPersonaDeContacto.setForeground(Color.ORANGE);
		txtpnPersonaDeContacto.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnPersonaDeContacto.setBackground(Color.BLACK);
		txtpnPersonaDeContacto.setBounds(500, 131, 157, 33);
		contentPane.add(txtpnPersonaDeContacto);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(669, 131, 252, 22);
		contentPane.add(textField_3);
		
		JTextPane txtpnTelefono = new JTextPane();
		txtpnTelefono.setText("telefono");
		txtpnTelefono.setForeground(Color.ORANGE);
		txtpnTelefono.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnTelefono.setBackground(Color.BLACK);
		txtpnTelefono.setBounds(500, 180, 72, 33);
		contentPane.add(txtpnTelefono);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(570, 189, 121, 22);
		contentPane.add(textField_4);
		
		JTextPane textPane = new JTextPane();
		textPane.setText("telefono");
		textPane.setForeground(Color.ORANGE);
		textPane.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		textPane.setBackground(Color.BLACK);
		textPane.setBounds(667, 8, 72, 33);
		contentPane.add(textPane);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(737, 17, 121, 22);
		contentPane.add(textField_5);
		
		JTextPane txtpnNivelDeUrgencia = new JTextPane();
		txtpnNivelDeUrgencia.setText("NIVEL DE URGENCIA");
		txtpnNivelDeUrgencia.setForeground(Color.ORANGE);
		txtpnNivelDeUrgencia.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		txtpnNivelDeUrgencia.setBackground(Color.BLACK);
		txtpnNivelDeUrgencia.setBounds(489, 226, 109, 56);
		contentPane.add(txtpnNivelDeUrgencia);
	}
}
