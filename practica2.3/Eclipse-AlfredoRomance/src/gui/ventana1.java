package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class ventana1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana1 frame = new ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 888, 438);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("file");
		menuBar.add(mnFile);
		
		JMenu mnGuardar = new JMenu("guardar");
		mnFile.add(mnGuardar);
		
		JMenu mnGuardarComo = new JMenu("Guardar Como");
		mnFile.add(mnGuardarComo);
		
		JMenu mnNewMenu = new JMenu("New menu");
		mnGuardarComo.add(mnNewMenu);
		
		JMenu mnAbrir = new JMenu("Abrir");
		mnFile.add(mnAbrir);
		
		JMenuBar menuBar_1 = new JMenuBar();
		mnFile.add(menuBar_1);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenu mnCortar = new JMenu("cortar");
		mnEdit.add(mnCortar);
		
		JMenu mnPegar = new JMenu("pegar");
		mnEdit.add(mnPegar);
		
		JMenu mnSeleccionar = new JMenu("seleccionar");
		mnEdit.add(mnSeleccionar);
		
		JMenu mnCopiar = new JMenu("copiar");
		mnEdit.add(mnCopiar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
